module.exports = {
  up: function(migration, DataTypes, done) {
    migration.addColumn('transactions', 'userId', DataTypes.INTEGER);
    done()
  },
  down: function(migration, DataTypes, done) {
    migration.removeColumn('transactions', 'userId');
    done()
  }
}
