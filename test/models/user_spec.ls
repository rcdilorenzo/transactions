expect = require 'expect.js'
User = require '../../source/models/user'
Auth = require '../../source/helpers/authentication'
test = it

createUser = ->
  User.new('Christian', 'Di Lorenzo', 'rcdilorenzo', 'test')

afterEach ->
  User.sync {+force}

describe 'user' ->

  test 'creates user with password and token' ->
    createUser().then (user) ->
      expect(user.password).not.to.eql 'test'
      expect(user.token).to.be.ok()

  test 'authenticates with password' ->
    createUser().then (user) ->
      expect(user.password).not.to.eql 'test'
      expect(user.checkPassword('test')).to.be true
      expect(user.checkPassword('other')).to.be false

  test 'doesn\'t create duplicates' ->
    createUser().then (_user) ->
      expect(_user).to.be.ok()
      createUser().then (user) ->
        expect().fail 'user should not have been created'
      , (msg) ->
        expect(msg).to.eql 'Username already taken.'


describe 'basic-authentication' ->

  test 'passes with valid user' ->
    createUser().then (user) ->
      Auth.basic 'rcdilorenzo', 'test', (_, valid) ->
        expect(valid).to.be true

  test 'fails with invalid user' ->
    createUser().then (user) ->
      Auth.basic 'rcdilorenzo', 'testing', (_, valid) ->
        expect(valid).to.be false


describe 'token-authentication' ->

  test 'returns user with valid token' ->
    createUser().then (user) ->
      Auth.token user.token, (_, valid, _user) ->
        expect(_user.id).to.eql user.id
        expect(valid).to.be.ok()
