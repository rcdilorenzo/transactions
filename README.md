Transactions API
================

For actual usage of the API on the client side, see http://rcdilorenzo.github.io/transactions/.

# Install/Setup

1. Make sure node is installed along with grunt
2. Run `npm install` followed by `grunt setup`
3. Create the dev and test databases locally as declared in `config.json`
4. Modify `config.json` with the database username/password information

NOTE: Whenever a grunt task is run, the other tasks are listed out with simple documentation. You can also run `grunt` and this documentation will include the current database information for the `NODE_ENV`.

# Testing

1. Ensure livescript is installed
2. `grunt test` your way to success!

# Running

1. Keep `grunt watch` running before starting the server (This compiles the livescript)
2. Run `node .` to start up the server
