module.exports = (server) ->

  server.get = (path, handler) ->
    server.route {
      method: 'GET'
      path: path
      handler: handler
    }

  server.post = (path, handler) ->
    server.route {
      method: 'POST'
      path: path
      handler: handler
    }

  server.getAuth = (path, auth, handler) ->
    server.route {
      method: 'GET'
      path: path
      config: {auth: auth}
      handler: handler
    }

  server.postAuth = (path, auth, handler) ->
    server.route {
      method: 'POST'
      path: path
      config: {auth: auth}
      handler: handler
    }
