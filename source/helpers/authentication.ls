User = require '../models/user'

Authentication =
  basic: (username, password, callback) ->
    User.find({ where: {username: username} }).then (user) ->
      return callback null, false if !user
      callback(null, user.checkPassword(password), user)
    , (err) ->
      console.log 'err', err
      callback null, false

  token: (token, callback) ->
    User.find({ where: {token: token} }).then (user) ->
      callback null, !!user, user

module.exports = Authentication
