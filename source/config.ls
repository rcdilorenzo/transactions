fs = require 'fs'
NODE_ENV = process.env.NODE_ENV || 'development'

config = __dirname + '/../config/config.json'

module.exports = JSON.parse(fs.readFileSync config, 'utf8')[NODE_ENV]
