Sequelize = require 'sequelize'
config = require './config'

sequelize = new Sequelize config.database, config.username, config.password, {
  dialect: 'postgres'
  logging: config.logging
}

sequelize.authenticate().complete (err) ->
  if !!err
    console.log 'Unable to connect to the database:', err

module.exports = sequelize
