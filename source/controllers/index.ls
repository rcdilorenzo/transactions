module.exports = (server) ->
  require('./users_controller')(server)
  require('./transactions_controller')(server)
