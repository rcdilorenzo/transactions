User = require '../models/user'
Transaction = require '../models/transaction'

module.exports = (server) ->

  server.getAuth '/transactions', 'token', (req, reply) ->
    user = req.auth.credentials
    user.getTransactions().then (transactions) ->
      { +authenticated, transactions: transactions.map (.asJSON()) } |> reply

  server.postAuth '/transactions', 'token', (req, reply) ->
    p = req.payload
    Transaction.create({
      title: p.title
      description: p.description
      price: p.price
      userId: req.auth.credentials.id
    }).then (transaction) ->
      { transaction: transaction.asJSON() } |> reply

