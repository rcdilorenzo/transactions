Hapi = require 'hapi'
User = require '../models/user'
config = require '../config'

module.exports = (server) ->

  server.postAuth '/login', 'simple', (req, reply) ->
    if req.query.token == config.authToken
      { user: req.auth.credentials.asJSON() } |> reply
    else
      reply(Hapi.error.unauthorized('Invalid login'));


  server.post '/signup', (req, reply) ->
    return reply(Hapi.error.unauthorized('Invalid login')) if req.query.token != config.authToken

    p = req.payload
    User.new(p.firstName, p.lastName, p.username, p.password).then (user) ->
      { user: user.asJSON() } |> reply
    , (error) ->
      Hapi.error.badRequest(error) |> reply
