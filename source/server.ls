Hapi = require 'hapi'
server = new Hapi.Server(3000)
Basic = require 'hapi-auth-basic'
Token = require 'hapi-auth-bearer-token'
require('./helpers/route')(server)

User = require './models/user'
Auth = require './helpers/authentication'
Transaction = require './models/transaction'

server.start ->
  console.log 'Server running at:', server.info.uri

server.pack.register Basic, ->
  server.auth.strategy 'simple', 'basic', { validateFunc: Auth.basic }

server.pack.register Token, ->
  server.auth.strategy 'token', 'bearer-access-token', {
    allowQueryToken: true
    accessTokenName: 'token'
    validateFunc: Auth.token
  }

require('./controllers')(server)
