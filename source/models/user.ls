Sequelize = require 'sequelize'
database = require '../database'
randtoken = require 'rand-token'
bcrypt = require 'bcrypt-nodejs'
Promise = require 'bluebird'

Transaction = require './transaction'

User = database.define 'user', {
  firstName: Sequelize.STRING
  lastName: Sequelize.STRING
  username: Sequelize.STRING
  password: Sequelize.STRING
  token: Sequelize.STRING
}, {
  classMethods:
    new: (firstName, lastName, username, password) ->
      User.count({ where: {username: username} }).then (count) ->
        return Promise.reject('Username already taken.') if count > 0
        User.create {
          firstName: firstName
          lastName: lastName
          username: username
          password: bcrypt.hashSync(password)
          token: randtoken.generate(20)
        }

    findByUsername: (username) ->
      User.find { where: { username: username } }

  instanceMethods:
    checkPassword: (password) ->
      bcrypt.compareSync(password, @password)

    asJSON: ->
      {
        id: @id
        token: @token
        firstName: @firstName
        lastName: @lastName
      }
}

User.hasMany(Transaction, as: 'transactions')

module.exports = User
