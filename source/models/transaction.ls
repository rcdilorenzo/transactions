Sequelize = require 'sequelize'
database = require '../database'

Transaction = database.define 'transaction', {
  title: Sequelize.STRING
  description: Sequelize.STRING
  company: Sequelize.STRING
  price: Sequelize.FLOAT
}, {
  classMethods:
    findOrCreateByTitle: (title) ->
      Transaction.findOrCreate { where: { title: title, price: 0.01 } }

  instanceMethods:
    asJSON: ->
      id: @id
      title: @title
      description: @description
      company: @company
      price: @price
}

module.exports = Transaction
