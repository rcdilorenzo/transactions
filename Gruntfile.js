var config = {};
try {
  config = require('./.js/config');
} catch (err) {
  console.log('err', err);
  console.log('Please run \'grung setup\' and \'grunt build\' before running database tasks');
}

module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt)

  grunt.initConfig({
    jsDir: '.js/',
    sourceDir: 'source/',
    shell: {
      watch: {
        command: 'echo "Watching..."; fswatch <%= sourceDir %> "rm -r <%= jsDir %>; mkdir <%= jsDir %>; lsc -cob <%= jsDir %> <%= sourceDir %>"'
      },
      build: {
        command: 'rm -r <%= jsDir %>; mkdir <%= jsDir %>; lsc -cdob <%= jsDir %> <%= sourceDir %>'
      },
      setup: {
        command: [
          'cp -n config/.config-example.json config/config.json'
        ].join('&&')
      },
      test: {
        command: 'NODE_ENV=test mocha --compilers ls:livescript'
      }
    },

    sequelize: {
      options: {
        migrationspath: __dirname + '/migrations',
        dialect:  'postgres',
        username: config.username,
        database: config.database,
        host:     config.host
      }
    }
  });

  function log(msg, bullet) {
    var prefix = bullet ? ' * ' : '';
    var task = grunt.cli.tasks[0] + ':';
    if (msg.indexOf(task) == -1) {
      grunt.log.writeln(prefix + msg);
    } else {
      grunt.log.oklns(grunt.log.wordlist([msg], {color: 'green'}));
    }
  }

  grunt.registerTask('default', null, function() {
    if (grunt.cli.tasks.length == 0) {
      var env = process.env.NODE_ENV;
      env = env == undefined ? 'development' : env;
      log('\n=== Database Information ===');
      log('env:       ' + env, true);
      log('database:  ' + config.database, true);
      log('username:  ' + config.username, true);
      log('password:  ' + config.password, true);
    }

    log('\n=== Tasks ===');
    log('setup:  Copies the example config to the correct path', true);
    log('build:  Compiles livescript (\'' + grunt.config.get('sourceDir') + '\') to javascipt (\'' + grunt.config.get('jsDir') + '\')', true);
    log('watch:  Watches and compiles livescript (\'' + grunt.config.get('sourceDir') + '\') to javascipt (\'' + grunt.config.get('jsDir') + '\')', true);
    log('test:   Runs mocha tests in the \'test/\' directory', true);
    log('');
    log('sequelize:migrate - Runs database migrations using NODE_ENV', true);
  });

  grunt.registerTask('setup', ['default', 'shell:setup']);
  grunt.registerTask('build', ['default', 'shell:build']);
  grunt.registerTask('watch', ['default', 'shell:watch']);
  grunt.registerTask('test', ['default', 'shell:test']);
}
